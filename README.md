## Muhimbi PDF test
###### Geoffrey Xue
An example of converting a word file into a pdf, and then watermarking it.
All code comes from the java [Muhimbi Samples](https://github.com/Muhimbi/PDF-Converter-Services-Online/tree/master/clients/v1/java)

```
cd client
sh build.sh
cd ..
cd convert
sh build_and_run.sh
```